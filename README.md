# chuck-jokes-golang

Тестовое задание Необходимо реализовать CLI-приложение для работы с https://api.chucknorris.io/. Приложение должно уметь: печатать на экран случайную шутку; выгружать по n случайных уникальных шуток ( n задается пользователем, дефолтное значение 5) для каждой из существующих категорий и сохранять их в текстовые файлы - по одному на каждую из категорий.

Пример Получение случайной шутки: Выгрузка шуток по категориям:

Требования решение должно быть в git-репозитории (можно прислать архив или опубликовать на github, gitlab, bitbucket...). Пожелания документирование кода; тесты; использование статического анализатора (конфигурацию положить в репозиторий).

Вакансия: https://sez-innopolis.ru/vacancy/2960/

https://myte-2867b.firebaseapp.com/tasks/Z1UC9Nui0sQ5dPAXSKBr
